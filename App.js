import React from 'react';
import { Provider } from 'react-redux';
import configureStore from './src/store';
import fixTimer from './fixTimer';

import ChatApp from './src/components/ChatApp';

fixTimer();

const store = configureStore();

const App = () => (
  <Provider store={store}>
    <ChatApp />
  </Provider>
);

export default App;
