# SCHOOL PROJECT
# It is a React Native Chat thats works with Expo and Firebase
# We can do the following :
    - Signup with Email, password and Username
    - Login with Email and Password
    - Send Message in the chat
    - Send Photos from the Camera or Gallery to the chat
    - Edit your Photo and your Name
    - Disconnect

#Go into the directory of the project
> cd messenger-native

# Install dependencies
> npm install

# If you have issues installing firebase run this command
> npm install firebase --force

# Run the application
> npm start

# You will see a brower opening expo-cli at http://localhost:1900X
# set the production mode to "true" by clicking on it at right

# Branch your USB Cable from your Phone to your computer if you Use Local Connexion
# and click on "Run Android Android Device/Simulator"
# else use Tunnel

# Now open expo in your phone
# Sign in then tap on "SCAN QR CODE"

# scan the QR CODE that is on expo-cli (in the browser)

# the application will start on your phone.

# If you want to deploy the application on Expo, go back in the Expo-Cli
# then click on publish or republish project

# confirms by clicking again on publish project