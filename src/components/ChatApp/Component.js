import React from 'react';
import PropTypes from 'prop-types';
import { ActivityIndicator } from 'react-native';
import DashboardNavigator from '../DashboardNavigator';
import AuthScreen from '../AuthScreen';
import styles from './Styles';

const ChatAppComponent = (props) => {
  const { restoring, logged } = props;
  if (restoring) {
    return <ActivityIndicator style={styles.activityIndicator} />;
  } if (logged) {
    return <DashboardNavigator />;
  }
  return <AuthScreen />;
};

ChatAppComponent.propTypes = {
  restoring: PropTypes.bool.isRequired,
  logged: PropTypes.bool.isRequired,
};

export default ChatAppComponent;
