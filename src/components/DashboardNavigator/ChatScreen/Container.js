import React, { Component } from 'react';
import { Platform } from 'react-native';
import LogoutButton from './LogoutButton';
import ChatScreenAndroid from './Component.android';
import ChatScreenIOS from './Component.ios';

/* https://github.com/Intellicode/eslint-plugin-react-native/blob/master/docs/rules/split-platform-components.md
   pas un problème de faire comme ça */

// ou faire un if
// eslint-disable-next-line import/no-unresolved

class ChatScreenContainer extends Component {
  static navigationOptions = {
    title: 'chat',
    headerRight: <LogoutButton />,
  }

  render() {
    if (Platform.OS === 'ios') {
      return (
        <ChatScreenIOS />
      );
    }
    return (
      <ChatScreenAndroid />
    );
  }
}

export default ChatScreenContainer;
