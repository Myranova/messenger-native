import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ProfilScreenComponent from './Component';

const ProfilScreenContainer = props => (
  <ProfilScreenComponent
    {...props}
  />
);

const mapStateToProps = state => ({
  user: state.session.user,
});

ProfilScreenContainer.propTypes = {
  user: PropTypes.objectOf(PropTypes.object).isRequired, // en attendant -> maybe use shape,
};

export default connect(mapStateToProps, null)(ProfilScreenContainer);
