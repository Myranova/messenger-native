import React, { Component } from 'react';
import {
  Image, Text, View, Alert, TouchableOpacity, ActivityIndicator, TextInput,
} from 'react-native';
import { ImagePicker, Permissions } from 'expo';
import PropTypes from 'prop-types';
import firebase from 'firebase';
import styles from './Styles';

const uuidv4 = require('uuid/v4');

class ProfilComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      photo: '',
      loading: false,
      username: props.user.displayName,
    };

    this.changingName = (username) => {
      this.setState({ username });
    };

    this.submitNameEdition = () => {
      const { username } = this.state;
      const user = firebase.auth().currentUser;

      user.updateProfile({
        displayName: username,
      }).then(() => {
        Alert.alert('success modification username ! ');
      });
    };
  }

    componentDidMount = async () => {
      await Permissions.askAsync(Permissions.CAMERA);
      await Permissions.askAsync(Permissions.CAMERA_ROLL);
    }

    uploadImageAsync = async (uri) => {
      // Why are we using XMLHttpRequest? See:
      // https://github.com/expo/expo/issues/2402#issuecomment-443726662
      this.setState({
        loading: true,
      });
      const blob = await new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.onload = () => {
          resolve(xhr.response);
        };
        xhr.onerror = () => {
          reject(new TypeError('network request failed'));
        };
        xhr.responseType = 'blob';
        xhr.open('GET', uri, true);
        xhr.send(null);
      });

      const uniqueFileName = uuidv4();
      const storageRef = firebase.storage().ref('images/avatars').child(`${uniqueFileName}.jpg`);
      const snapshot = await storageRef.put(blob);
      blob.close();

      const downloadUrl = snapshot.ref.getDownloadURL().then((URL) => {
        const user = firebase.auth().currentUser;
        user.updateProfile({
          photoURL: URL,
        }).then(() => {
          Alert.alert('Profile photo successfully modified !');
          this.setState({
            loading: false,
          });
        }).catch((error) => {
          Alert.alert(error);
        });
      }).catch((error) => {
        Alert.alert(error);
      });
      return downloadUrl;
    }

      takePictureCamera = async () => {
        try {
          const result = await ImagePicker.launchCameraAsync({
            mediaTypes: 'Images',
            allowsEditing: false,
          });
          if (!result.cancelled) {
            this.setState({
              photo: result.uri,
            });
            this.uploadImageAsync(result.uri);
            // Alert.alert(user.photoURL);
            // CameraRoll.saveToCameraRoll(this.state.image);
          }
        } catch (error) {
          Alert.alert(error);
        }
      }

      takePictureLibrary = async () => {
        try {
          const result = await ImagePicker.launchImageLibraryAsync({
            allowsEditing: false,
            aspect: [4, 3],
          });
          if (!result.cancelled) {
            this.setState({
              photo: result.uri,
            });
            this.uploadImageAsync(result.uri);
          }
        } catch (error) {
          Alert.alert(error);
        }
      }

    render = () => {
      const { photo, loading } = this.state;
      const { user } = this.props;
      if (loading) {
        return (
          <View style={styles.container}>
            <View style={styles.header}>
              <View style={styles.headerContent}>
                <TouchableOpacity onPress={this.takePictureLibrary}>
                  <ActivityIndicator size="large" color="#0000ff" />
                </TouchableOpacity>
                <TextInput
                  style={styles.name}
                  placeholder="your username"
                  keyboardType="default"
                  value={user.displayName}
                />
                <Text
                  style={styles.userInfo}
                >
                  { user.email }
                </Text>
                <Text style={styles.userInfo}> France </Text>
              </View>
            </View>
          </View>
        );
      }
      return (
        <View style={styles.container}>
          <View style={styles.header}>
            <View style={styles.headerContent}>
              <TouchableOpacity onPress={this.takePictureLibrary}>
                {photo === '' ? (
                  <Image
                    style={styles.avatar}
                    source={{ uri: user.photoURL }}
                  />
                )
                  : (
                    <Image
                      style={styles.avatar}
                      source={{ uri: photo }}
                    />
                  ) }
              </TouchableOpacity>
              <TextInput
                style={styles.name}
                placeholder="your username"
                keyboardType="default"
                onChangeText={this.changingName}
                onSubmitEditing={this.submitNameEdition}
              >
                {user.displayName}
              </TextInput>
              <Text
                style={styles.userInfo}
              >
                {user.email}
              </Text>
              <TextInput style={styles.userInfo}> France </TextInput>
            </View>
          </View>
          <View style={styles.body} />
        </View>
      );
    }
}

ProfilComponent.propTypes = {
  user: PropTypes.objectOf(PropTypes.object).isRequired,
};

export default ProfilComponent;
