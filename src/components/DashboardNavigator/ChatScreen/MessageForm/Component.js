import React, { Component } from 'react';
import { ImagePicker, Permissions } from 'expo';
import {
  TextInput, TouchableOpacity, Image, Alert, KeyboardAvoidingView,
} from 'react-native';
import PropTypes from 'prop-types';
import firebase from '../../../../services/firebase';
import styles from './Styles';

const uuidv4 = require('uuid/v4');

const OPACITY_ENABLED = 1.0;
const OPACITY_DISABLED = 0.2;

const icCamera = require('../../../../images/ic_camera.png');
const icPhoto = require('../../../../images/ic_photo.png');
const icMessage = require('../../../../images/ic_send.png');

class MessageFormComponent extends Component {
  state = {
    hasCameraPermission: null,
    hasMediaAcessPermission: null,
  }

  async componentDidMount() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    await Permissions.askAsync(Permissions.CAMERA_ROLL);
    this.setState({
      hasCameraPermission: status === 'granted',
    });
  }

  componentDidUpdate(prevProps) {
    const { sendingError } = this.props;
    if (!prevProps.sendingError && sendingError) {
      Alert.alert('error', sendingError);
    }
  }

  handleMessageChange = (message) => {
    const { updateMessage } = this.props;
    updateMessage(message);
  };

  handleButtonPress = () => {
    const { sendMessage, message } = this.props;
    sendMessage(message, '');
  };

  uploadImageAsync = async (uri) => {
    // Why are we using XMLHttpRequest? See:
    // https://github.com/expo/expo/issues/2402#issuecomment-443726662

    // Alert.alert('enter upload Image Async');
    const blob = await new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.onload = () => {
        resolve(xhr.response);
      };
      xhr.onerror = () => {
        reject(new TypeError('network request failed'));
      };
      xhr.responseType = 'blob';
      xhr.open('GET', uri, true);
      xhr.send(null);
    });

    const uniqueFileName = uuidv4();
    const storageRef = firebase.storage().ref('images').child(`${uniqueFileName}.jpg`);
    const snapshot = await storageRef.put(blob);
    blob.close();

    const downloadUrl = snapshot.ref.getDownloadURL().then((URL) => {
      // console.log(URL); only for debug
      Alert.alert(URL);
      const { sendMessage } = this.props;
      sendMessage(URL, 'image');
    }).catch((error) => {
      Alert.alert('error', error);
    });
    return downloadUrl;
  }

  takePictureCamera = async () => {
    try {
      const result = await ImagePicker.launchCameraAsync({
        mediaTypes: 'Images',
        allowsEditing: false,
      });

      if (!result.cancelled) {
        // Alert.alert(result.uri);
        // CameraRoll.saveToCameraRoll(this.state.image);
        // Alert.alert(result.uri);
        this.uploadImageAsync(result.uri);
      }
    } catch (error) {
      Alert.alert(error);
    }
  }

  takePictureLibrary = async () => {
    try {
      const result = await ImagePicker.launchImageLibraryAsync({
        allowsEditing: false,
        aspect: [4, 3],
      });
      if (!result.cancelled) {
        // Alert.alert(result.uri);
        this.uploadImageAsync(result.uri);
      }
    } catch (error) {
      Alert.alert(error);
    }
  }

  render() {
    const {
      hasCameraPermission, hasMediaAcessPermission,
    } = this.state;
    if (hasCameraPermission === false) {
      Alert.alert('no camera permissions');
    } if (hasMediaAcessPermission === false) {
      Alert.alert('cant save picture on this phone');
    }
    const { sending, message } = this.props;
    const isButtonDisabled = sending || message.trim().length === 0;
    const opacity = isButtonDisabled ? OPACITY_DISABLED : OPACITY_ENABLED;

    return (
      <KeyboardAvoidingView
        style={styles.container}
        keyboardVerticalOffset="100"
        behavior="padding"
        enabled
      >
        <TextInput
          style={styles.textInput}
          placeholder="message"
          returnKeyType="send"
          onChangeText={this.handleMessageChange}
          value={message}
          underlineColorAndroid="transparent"
          editable={!sending}
        />

        <TouchableOpacity
          style={styles.button}
          onPress={this.handleButtonPress}
          disabled={isButtonDisabled}
        >

          <Image
            source={icMessage}
            style={{ opacity }}
          />

        </TouchableOpacity>

        <TouchableOpacity
          style={styles.button}
          onPress={this.takePictureCamera}
        >

          <Image
            style={{ width: 45, height: 45 }}
            source={icCamera}
          />

        </TouchableOpacity>

        <TouchableOpacity
          style={styles.button}
          onPress={this.takePictureLibrary}
        >

          <Image
            style={{ width: 45, height: 45 }}
            source={icPhoto}
          />

        </TouchableOpacity>

      </KeyboardAvoidingView>
    );
  }
}

MessageFormComponent.propTypes = {
  sending: PropTypes.bool.isRequired,
  sendMessage: PropTypes.func.isRequired,
  updateMessage: PropTypes.func.isRequired,
  message: PropTypes.string.isRequired,
  sendingError: PropTypes.string.isRequired,
  //  sendingError: PropTypes.string
};

export default MessageFormComponent;
