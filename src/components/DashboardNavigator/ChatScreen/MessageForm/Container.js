import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { sendMessage, updateMessage } from '../../../../store/chat';

import MessageForm from './Component';

const MessageFormContainer = props => (
  <MessageForm
    {...props}
  />
);

const mapStateToProps = state => ({
  sending: state.chat.sending,
  sendingError: state.chat.sendingError,
  message: state.chat.message,
  image: state.chat.image,
});

const mapDispatchToProps = {
  sendMessage,
  updateMessage,
};

MessageFormContainer.propTypes = {
  sending: PropTypes.bool.isRequired,
  sendMessage: PropTypes.func.isRequired,
  updateMessage: PropTypes.func.isRequired,
  message: PropTypes.string.isRequired,
  sendingError: PropTypes.string,
};

MessageFormContainer.defaultProps = {
  sendingError: 'error',
};

export default connect(mapStateToProps, mapDispatchToProps)(MessageFormContainer);
