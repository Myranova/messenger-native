import React from 'react';
import { KeyboardAvoidingView } from 'react-native';
import { TabNavigator } from 'react-navigation';
import ProfileScreen from './ProfilScreen';
import MessagesList from './MessagesList';
import MessageForm from './MessageForm';

import styles from './Styles';

const AppScreen = () => (
  <KeyboardAvoidingView
    style={styles.container}
    behavior="padding"
    keyboardVerticalOffset={64}
  >

    <MessagesList />
    <MessageForm />
  </KeyboardAvoidingView>
);

const routeConfigs = {
  Home: { screen: AppScreen },
  Profil: { screen: ProfileScreen },
};

const tabBarOptions = {
  tabBarOptions: {
    activeTintColor: '#88cc88',
    inactiveTintColor: '#aaaaaa',
    showIcon: true,
    scrollEnabled: true,
    indicatorStyle: {
      display: 'none',
    },
    style: {
      backgroundColor: '#ffffff',
    },
  },
  tabBarPosition: 'bottom',
};

export default TabNavigator(routeConfigs, tabBarOptions);
