import React from 'react';
import { View } from 'react-native';
import { TabNavigator } from 'react-navigation';
import ProfilScreen from './ProfilScreen';
import MessagesList from './MessagesList';
import MessageForm from './MessageForm';
import styles from './Styles';

const AppScreen = () => (
  <View style={styles.container}>
    <MessagesList />
    <MessageForm />
  </View>
);

const routeConfigs = {
  Home: { screen: AppScreen },
  Profil: { screen: ProfilScreen },
};

const tabBarOptions = {
  tabBarOptions: {
    activeTintColor: '#88cc88',
    inactiveTintColor: '#aaaaaa',
    showIcon: true,
    scrollEnabled: true,
    indicatorStyle: {
      display: 'none',
    },
    style: {
      backgroundColor: '#ffffff',
    },
  },
  tabBarPosition: 'bottom',
};

export default TabNavigator(routeConfigs, tabBarOptions);
