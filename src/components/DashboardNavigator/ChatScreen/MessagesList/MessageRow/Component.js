import React from 'react';
import {
  View, Text, Image, Animated, PanResponder,
} from 'react-native';
import PropTypes from 'prop-types';
import relativeDate from 'relative-date';

import styles from './Styles';

const MESSAGE_TEXT_MARGIN = 50;

class MessageRowComponent extends React.Component {
    state = {
      pan: new Animated.ValueXY(),
      scale: new Animated.Value(1),
    };

  componentWillMount = () => {
    const { pan, scale } = this.state;
    this.panResponder = PanResponder.create({
      onMoveShouldSetPanResponder: () => true,
      onMoveShouldSetPanResponderCapture: () => true,

      onPanResponderGrant: () => {
        /* eslint no-underscore-dangle: [2, { "allow": ["_value"]}] */
        pan.setOffset({ x: pan.x._value, y: pan.y._value });
        pan.setValue({ x: 0, y: 0 });
        Animated.spring(
          scale,
          { toValue: 1.75, friction: 3 },
        ).start();
      },

      onPanResponderMove: Animated.event([
        // null, { dx: pan.x, dy: pan.y },
      ]),

      onPanResponderRelease: () => {
        pan.flattenOffset();
        Animated.spring(scale, { toValue: 1, friction: 3 }).start();
      },

    });
  }

  render() {
    const rotate = '0deg';
    const { pan, scale } = this.state;
    // Calculate the x and y transform from the pan value
    const [translateX, translateY] = [pan.x, pan.y];

    // Calculate the transform property and set it as a value for
    // our style which we add below to the Animated.View component
    const imageStyle = { transform: [{ translateX }, { translateY }, { rotate }, { scale }] };

    const { message, isCurrentUser } = this.props;
    const alignItems = isCurrentUser ? 'flex-end' : 'flex-start';
    const margin = isCurrentUser ? { marginLeft: MESSAGE_TEXT_MARGIN }
      : { marginRight: MESSAGE_TEXT_MARGIN };
    const username = isCurrentUser ? 'you' : message.user.email;
    const date = relativeDate(new Date(message.createdAt));
    return (
      <View
        style={styles.container}
      >
        <View
          style={[styles.bubbleView, { alignItems }, margin]}
        >
          <Text
            style={styles.userText}
          >
            {`${date} - ${username}`}
          </Text>
          {message.typeData === 'image'
            ? (
              <View styles={styles.animated}>
                <Animated.View style={imageStyle} {...this.panResponder.panHandlers}>
                  <Image style={{ width: 250, height: 250 }} resizeMethod="resize" source={{ uri: message.text }} />
                </Animated.View>
              </View>
            )

            : (
              <Text
                style={styles.messageText}
              >
                {message.text}
              </Text>
            ) }
        </View>
      </View>
    );
  }
}

MessageRowComponent.propTypes = {
  isCurrentUser: PropTypes.bool.isRequired,
  message: PropTypes.shape({
    createdAt: PropTypes.number.isRequired,
    typeData: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    user: PropTypes.shape({
      email: PropTypes.string.isRequired,
    }),
  }),
};

MessageRowComponent.defaultProps = {
  message: 'default',
};

export default MessageRowComponent;
