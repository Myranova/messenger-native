import React from 'react';
import PropTypes from 'prop-types';

import MessageRow from './Component';

import firebaseService from '../../../../../services/firebase';

const MessageRowContainer = (props) => {
  const { message } = props;
  const isCurrentUser = firebaseService.auth().currentUser.email === message.user.email;
  return (
    <MessageRow
      message={message}
      isCurrentUser={isCurrentUser}
    />
  );
};

MessageRowContainer.propTypes = {
  message: PropTypes.objectOf(PropTypes.object).isRequired,
};

export default MessageRowContainer;
