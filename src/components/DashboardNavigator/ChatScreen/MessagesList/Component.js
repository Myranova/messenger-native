import React, { Component } from 'react';
import { FlatList, Text } from 'react-native';
import PropTypes from 'prop-types';
import MessageRow from './MessageRow';
import styles from './Styles';

const ITEM_HEIGHT = 50;

class MessageListComponent extends Component {
  constructor() {
    super();

    this.renderItem = ({ item }) => <MessageRow message={item} />;

    this.emptyList = () => (
      <Text
        style={styles.placeholder}
      >
        placeholder
      </Text>
    );

    this.itemLayout = (data, index) => (
      { length: ITEM_HEIGHT, offset: ITEM_HEIGHT * index, index }
    );
  }

  componentDidUpdate() {
    const { data } = this.props;
    if (data.length) {
      this.flatList.scrollToIndex({ animated: true, index: 0 });
    }
  }

  render() {
    const { data } = this.props;
    const contentContainerStyle = data.length ? null : styles.flatlistContainerStyle;
    return (
      <FlatList
        ref={(c) => { this.flatList = c; }}
        style={styles.container}
        contentContainerStyle={contentContainerStyle}
        data={data}
        keyExtractor={item => item.time}
        renderItem={this.renderItem}
        getItemLayout={this.itemLayout}
        ListEmptyComponent={this.emptyList}
        inverted
      />
    );
  }
}

MessageListComponent.propTypes = {
  data: PropTypes.arrayOf(PropTypes.array).isRequired,
};

export default MessageListComponent;
