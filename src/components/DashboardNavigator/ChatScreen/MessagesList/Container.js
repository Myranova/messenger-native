import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { loadMessages } from '../../../../store/chat/actions';
import getChatItems from '../../../../store/chat/getChatItems';

import MessageListComponent from './Component';

class MessagesListContainer extends Component {
  componentDidMount() {
    const { loadMessagesProps } = this.props;
    loadMessagesProps();
  }

  render() {
    const { messages } = this.props;
    const data = getChatItems(messages).reverse();
    return (
      <MessageListComponent
        data={data}
      />
    );
  }
}

const mapStateToProps = state => ({
  messages: state.chat.messages,
  error: state.chat.loadMessagesError,
});

const mapDispatchToProps = {
  loadMessagesProps: loadMessages,
};

MessagesListContainer.propTypes = {
  messages: PropTypes.objectOf(PropTypes.object).isRequired, // en attendant
  // error: PropTypes.string,
  loadMessagesProps: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(MessagesListContainer);
