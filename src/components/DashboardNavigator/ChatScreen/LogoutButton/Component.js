import React from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, Image } from 'react-native';

import styles from './Styles';

const icExittoAppPng = require('../../../../images/ic_exit_to_app.png');

const LogoutButtonComponent = (props) => {
  const { logout } = props;
  return (
    <TouchableOpacity
      style={styles.container}
      onPress={logout}
    >
      <Image source={icExittoAppPng} />
    </TouchableOpacity>
  );
};

LogoutButtonComponent.propTypes = {
  logout: PropTypes.func.isRequired,
};

export default LogoutButtonComponent;
