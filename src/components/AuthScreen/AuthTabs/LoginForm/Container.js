import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Image } from 'react-native';

import { connect } from 'react-redux';
import { loginUser } from '../../../../store/session';
import LoginFormComponent from './Component';

const icPersonOutline = require('../../../../images/ic_person_outline.png');

class LoginFormContainer extends Component {
  static navigationOptions = {
    tabBarLabel: 'login',
    tabBarIcon: ({ tintColor }) => (
      <Image
        source={icPersonOutline}
        style={{ tintColor }}
      />
    ),
  }

  render() {
    const { login } = this.props;
    return (
      <LoginFormComponent
        login={login}
      />
    );
  }
}

LoginFormContainer.propTypes = {
  login: PropTypes.func.isRequired,
};

const mapDispatchToProps = {
  login: loginUser,
};

export default connect(null, mapDispatchToProps)(LoginFormContainer);
