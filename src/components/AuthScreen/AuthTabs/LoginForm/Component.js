import React from 'react';
import PropTypes from 'prop-types';

import BasicFormLogin from '../BasicFormLogin';

const LoginFormComponent = (props) => {
  const { login } = props;
  return (
    <BasicFormLogin
      buttonTitle="login"
      onButtonPress={login}
    />
  );
};

LoginFormComponent.propTypes = {
  login: PropTypes.func.isRequired,
};

export default LoginFormComponent;
