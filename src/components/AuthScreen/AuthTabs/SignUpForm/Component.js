import React from 'react';
import PropTypes from 'prop-types';

import BasicFormSignup from '../BasicFormSignup';

const SignUpFormComponent = (props) => {
  const { signup } = props;
  return (
    <BasicFormSignup
      buttonTitle="signup"
      onButtonPress={signup}
    />
  );
};

SignUpFormComponent.propTypes = {
  signup: PropTypes.func.isRequired,
};

export default SignUpFormComponent;
