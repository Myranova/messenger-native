import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View, TextInput, TouchableOpacity, Text,
} from 'react-native';

import styles from './Styles';

class BasicFormComponent extends Component {
  constructor(props) {
    super(props);
    this.state = { email: '', password: '', imageUri: '' };

    this.handleEmailChange = (email) => {
      this.setState({ email });
    };

    this.handlePasswordChange = (password) => {
      this.setState({ password });
    };

    this.handleButtonPress = () => {
      const { onButtonPress } = this.props;
      const { email, password, imageUri } = this.state;
      onButtonPress(email, password, imageUri);
    };
  }

  render() {
    const { buttonTitle } = this.props;
    const { email, password } = this.state;
    return (
      <View
        style={styles.container}
      >

        <TextInput
          style={styles.textInput}
          placeholder="email"
          returnKeyType="next"
          keyboardType="email-address"
          autoCapitalize="none"
          onChangeText={this.handleEmailChange}
          value={email}
          underlineColorAndroid="transparent"
        />

        <TextInput
          style={styles.textInput}
          placeholder="password"
          secureTextEntry
          returnKeyType="done"
          onChangeText={this.handlePasswordChange}
          value={password}
          underlineColorAndroid="transparent"
        />

        <TouchableOpacity
          style={styles.button}
          onPress={this.handleButtonPress}
        >

          <Text style={styles.buttonTitle}>{buttonTitle}</Text>

        </TouchableOpacity>

      </View>
    );
  }
}

BasicFormComponent.propTypes = {
  buttonTitle: PropTypes.string.isRequired,
  onButtonPress: PropTypes.func.isRequired,
};

export default BasicFormComponent;
