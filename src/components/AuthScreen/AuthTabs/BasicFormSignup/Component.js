import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View, TextInput, TouchableOpacity, Text,
} from 'react-native';

import styles from './Styles';

class BasicFormComponent extends Component {
    state = {
      email: '', password: '', username: '',
    };


  handleUsernameChange = (username) => {
    this.setState({ username });
  }

  handleEmailChange = (email) => {
    this.setState({ email });
  };

  handlePasswordChange = (password) => {
    this.setState({ password });
  };

  handleButtonPress = () => {
    const { onButtonPress } = this.props;
    const { email, password, username } = this.state;
    onButtonPress(email, password, username);
  };

  render() {
    const { buttonTitle } = this.props;
    const { email, password, username } = this.state;
    return (
      <View
        style={styles.container}
      >
        <TextInput
          style={styles.textInput}
          placeholder="username"
          returnKeyType="next"
          keyboardType="default"
          autoCapitalize="none"
          onChangeText={this.handleUsernameChange}
          value={username}
          underlineColorAndroid="transparent"
        />

        <TextInput
          style={styles.textInput}
          placeholder="email"
          returnKeyType="next"
          keyboardType="email-address"
          autoCapitalize="none"
          onChangeText={this.handleEmailChange}
          value={email}
          underlineColorAndroid="transparent"
        />

        <TextInput
          style={styles.textInput}
          placeholder="password"
          secureTextEntry
          returnKeyType="done"
          onChangeText={this.handlePasswordChange}
          value={password}
          underlineColorAndroid="transparent"
        />

        <TouchableOpacity
          style={styles.button}
          onPress={this.handleButtonPress}
        >

          <Text style={styles.buttonTitle}>{buttonTitle}</Text>
        </TouchableOpacity>

      </View>
    );
  }
}

BasicFormComponent.propTypes = {
  buttonTitle: PropTypes.string.isRequired,
  onButtonPress: PropTypes.func.isRequired,
};

export default BasicFormComponent;
