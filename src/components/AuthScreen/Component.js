import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Alert } from 'react-native';

import AuthTabs from './AuthTabs';
import LoadingIndicator from './LoadingIndicator';

import styles from './Styles';

class AuthScreenComponent extends Component {
  componentDidUpdate(prevProps) {
    const { error } = this.props;
    if (!prevProps.error && error) {
      Alert.alert('error', error);
    }
  }

  render() {
    const { loading } = this.props;
    return (
      <View style={styles.container}>
        <AuthTabs />
        {loading && <LoadingIndicator />}
      </View>
    );
  }
}

AuthScreenComponent.propTypes = {
  loading: PropTypes.bool.isRequired,
  error: PropTypes.string.isRequired,
  // error: PropTypes.string
};

export default AuthScreenComponent;
