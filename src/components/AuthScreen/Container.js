import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import AuthScreenComponent from './Component';

const AuthScreenContainer = props => (
  <AuthScreenComponent
    {...props}
  />
);

const mapStateToProps = state => ({
  loading: state.session.loading,
  error: state.session.error,
});

AuthScreenContainer.propTypes = {
  loading: PropTypes.bool.isRequired,
  error: PropTypes.string,
};

AuthScreenContainer.defaultProps = {
  error: 'error',
};

export default connect(mapStateToProps)(AuthScreenContainer);

/* facile donc mapSTateToProps récupère les states qu'on a besoin et
   les envoie en props au component rien de plus
   et dispatch fait un rerender car il modifie le state */
