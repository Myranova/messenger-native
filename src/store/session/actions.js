import { Alert } from 'react-native';
import * as types from './actionTypes';
import firebaseService from '../../services/firebase';

const sessionRestoring = () => ({
  type: types.SESSION_RESTORING,
});

const sessionLoading = () => ({
  type: types.SESSION_LOADING,
});

const sessionSuccess = user => ({
  type: types.SESSION_SUCCESS,
  user,
});

const sessionError = error => ({
  type: types.SESSION_ERROR,
  error,
});

const sessionLogout = () => ({
  type: types.SESSION_LOGOUT,
});

const updateProfileAction = user => ({
  type: types.UPDATE_PROFILE,
  user,
});

export const updateProfile = user => (dispatch) => {
  dispatch(updateProfileAction(user));
};

export const restoreSession = () => (dispatch) => {
  dispatch(sessionRestoring());

  const unsubscribe = firebaseService.auth()
    .onAuthStateChanged((user) => {
      if (user) {
        dispatch(sessionSuccess(user));
        unsubscribe();
      } else {
        dispatch(sessionLogout());
        unsubscribe();
      }
    });
};

export const loginUser = (email, password) => (dispatch) => {
  dispatch(sessionLoading());

  firebaseService.auth()
    .signInWithEmailAndPassword(email, password)
    .catch((error) => {
      dispatch(sessionError(error.message));
    }).then(() => {
    });

  const unsubscribe = firebaseService.auth()
    .onAuthStateChanged((user) => {
      if (user) {
        dispatch(sessionSuccess(user));
        unsubscribe();
      }
    });
};

export const signupUser = (email, password, username) => (dispatch) => {
  dispatch(sessionLoading());

  firebaseService.auth()
    .createUserWithEmailAndPassword(email, password)
    .then((user) => {
      if (user) {
        user.updateProfile({
          displayName: username,
          photoURL: 'https://bootdey.com/img/Content/avatar/avatar6.png',
        }).then(() => {
          Alert.alert('Welcome to the chat !');
        });
      }
    })
    .catch((error) => {
      dispatch(sessionError(error.message));
    });

  const unsubscribe = firebaseService.auth()
    .onAuthStateChanged((user) => {
      if (user) {
        dispatch(sessionSuccess(user));
        unsubscribe();
      }
    });
};

export const logoutUser = () => (dispatch) => {
  dispatch(sessionLoading());

  firebaseService.auth()
    .signOut()
    .then(() => {
      dispatch(sessionLogout());
    })
    .catch((error) => {
      dispatch(sessionError(error.message));
    });
};
