import * as types from './actionTypes';
import firebaseService from '../../services/firebase';

const FIREBASE_REF_MESSAGES = firebaseService.database().ref('Messages');
const FIREBASE_REF_MESSAGES_LIMIT = 20;

const chatCameraLoading = () => ({
  type: types.CHAT_LOAD_CAMERA,
});

const chatMessageLoading = () => ({
  type: types.CHAT_MESSAGE_LOADING,
});

const chatMessageSuccess = () => ({
  type: types.CHAT_MESSAGE_SUCCESS,
});

const chatMessageError = error => ({
  type: types.CHAT_MESSAGE_ERROR,
  error,
});

const chatUpdateMessage = text => ({
  type: types.CHAT_MESSAGE_UPDATE,
  text,
});

const loadMessagesSuccess = messages => ({
  type: types.CHAT_LOAD_MESSAGES_SUCCESS,
  messages,
});

const loadMessagesError = error => ({
  type: types.CHAT_LOAD_MESSAGES_ERROR,
  error,
});

export const sendMessage = (message, typeData) => (dispatch) => {
  dispatch(chatMessageLoading());

  const { currentUser } = firebaseService.auth();
  const createdAt = new Date().getTime();
  const chatMessage = {
    text: message,
    typeData,
    createdAt,
    user: {
      id: currentUser.uid,
      email: currentUser.email,
    },
  };

  FIREBASE_REF_MESSAGES.push().set(chatMessage, (error) => {
    if (error) {
      dispatch(chatMessageError(error.message));
    } else {
      dispatch(chatMessageSuccess());
    }
  });
};

export const updateMessage = text => (dispatch) => {
  dispatch(chatUpdateMessage(text));
};

export const loadMessages = () => (dispatch) => {
  FIREBASE_REF_MESSAGES.limitToLast(FIREBASE_REF_MESSAGES_LIMIT).on('value', (snapshot) => {
    dispatch(loadMessagesSuccess(snapshot.val()));
  }, (errorObject) => {
    dispatch(loadMessagesError(errorObject.message));
  });
};

export const loadCamera = () => (dispatch) => {
  dispatch(chatCameraLoading);
};
