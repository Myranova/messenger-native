const getChatItems = data => (data ? Object.keys(data).map(key => data[key]) : []);

export default getChatItems;
